Billy
==

Integration between Drupal Commerce and the Danish accounting service provider <a href="http://billy.dk">Billy</a>.

<h3>Under active development</h3>

This module is being developed right now.

<h3>Requirements</h3>

<ul>
  <li><a href="http://drupal.org/project/libraries">libraries</a>
  <li><a href="https://github.com/lsolesen/billy-php-sdk">Billy PHP SDK</a>
</ul>

<h3>Sponsors</h3>

<a href="http://discimport.dk">discimport.dk</a>