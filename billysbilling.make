api = 2
core = 7.x

libraries[billy][download][type] = "git"
libraries[billy][download][url] = "https://github.com/lsolesen/billy-php-sdk.git"
libraries[billy][directory_name] = "billy-php-sdk"
libraries[billy][destination] = "libraries"
libraries[billy][branch] = "master"
libraries[billy][revision] = "5abf35e"
